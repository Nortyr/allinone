package com.mountain.monk.demo;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.state.ConnectionState;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.curator.utils.CloseableUtils;
import org.apache.curator.x.discovery.ServiceCache;
import org.apache.curator.x.discovery.ServiceDiscovery;
import org.apache.curator.x.discovery.ServiceDiscoveryBuilder;
import org.apache.curator.x.discovery.ServiceInstance;
import org.apache.curator.x.discovery.details.ServiceCacheListener;

/**
 * User: hupeng
 * Date: 14-9-16
 * Time: 下午8:16
 */
public class ClientApp {
    private static final String BASE_PATH = "/my/services";

    private static final String ZOOKEEPER_CONNECT_STRING = "localhost:2181";

    public static void main(String[] args) throws Exception {
        // 创建 Curator 客户端
        CuratorFramework client = CuratorFrameworkFactory.newClient(ZOOKEEPER_CONNECT_STRING, new ExponentialBackoffRetry(1000, 3));
        client.start();

        // 创建 ServiceDiscovery 实例并启动
        ServiceDiscovery<Object> serviceDiscovery = ServiceDiscoveryBuilder.builder(Object.class)
                .basePath(BASE_PATH)
                .client(client)
                .build();

        serviceDiscovery.start();

        // 创建服务缓存并添加监听器
        ServiceCache<Object> cache = serviceDiscovery.serviceCacheBuilder()
                .name("my-service-name")
                .build();

        cache.addListener(new ServiceCacheListener() {
            @Override
            public void cacheChanged() {
                // 服务发现事件发生时触发此方法
                for (ServiceInstance<Object> instance : cache.getInstances()) {
                    // 处理服务实例的变更事件
                    System.out.println("Service Instance: " + instance.getId());
                }
            }

            @Override
            public void stateChanged(CuratorFramework client, ConnectionState newState) {
                // 处理 Curator 客户端状态变更事件
                System.out.println("Client State Changed: " + newState);
            }
        });

        // 启动服务缓存
        cache.start();

        // 在这里可以进行其他操作，例如查询服务实例列表
        Thread.sleep(Integer.MAX_VALUE);
        // 最后，记得关闭资源
        cache.close();
        serviceDiscovery.close();
        client.close();
    }
}