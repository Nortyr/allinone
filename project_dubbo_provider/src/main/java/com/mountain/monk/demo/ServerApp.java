package com.mountain.monk.demo;


import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.curator.x.discovery.ServiceDiscovery;
import org.apache.curator.x.discovery.ServiceDiscoveryBuilder;
import org.apache.curator.x.discovery.ServiceInstance;
import org.apache.curator.x.discovery.UriSpec;


import java.util.UUID;

/**
 * User: hupeng
 * Date: 14-9-16
 * Time: 下午8:05
 */
public class ServerApp {
    private static final String ZOOKEEPER_CONNECT_STRING = "localhost:2181";
    private static final String BASE_PATH = "/my/services";

    public static void main(String[] args) throws Exception {
        // 创建 Curator 客户端
        CuratorFramework client = CuratorFrameworkFactory.newClient(ZOOKEEPER_CONNECT_STRING, new ExponentialBackoffRetry(1000, 3));
        client.start();

        // 创建 ServiceDiscovery 实例
        ServiceDiscovery<Object> serviceDiscovery = ServiceDiscoveryBuilder.builder(Object.class)
                .basePath(BASE_PATH)
                .client(client)
                .build();

        // 启动 ServiceDiscovery
        serviceDiscovery.start();

        //evaluate

        // 创建服务实例
        ServiceInstance<Object> serviceInstance = ServiceInstance.builder()
                .name("my-service-name") // 服务名称
                .address("192.168.1.100") // 服务 IP 地址
                .port(8080) // 服务端口
                .build();

        // 注册服务实例
        serviceDiscovery.registerService(serviceInstance);



        Thread.sleep(Integer.MAX_VALUE);

    }
}
