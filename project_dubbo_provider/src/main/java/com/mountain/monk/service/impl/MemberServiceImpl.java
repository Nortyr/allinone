package com.mountain.monk.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.mountain.monk.IMemberService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @Author wulongbo
 * @Date 2021/1/5 19:39
 * @Version 1.0
 */
@DubboService
@Component
public class MemberServiceImpl implements IMemberService {

    @Value("${dubbo.protocol.port}")
    private String dubboPort;

    @Autowired
    TestServiceImpl testService;

    @Override
    public String getUser() {
        System.out.println(testService.test());
        System.out.println("订单服务调用会员服务...dubbo服务端口号：" + dubboPort);
        return "订单服务调用会员服务...dubbo服务端口号：" + dubboPort;
    }
}
