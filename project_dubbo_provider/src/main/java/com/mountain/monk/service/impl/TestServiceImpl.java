package com.mountain.monk.service.impl;

import com.mountain.monk.TestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class TestServiceImpl {
    @Autowired
    private TestMapper testMapper;

    @Transactional
    public String test(){
        try {
            Thread.sleep(10000L);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        testMapper.insertTest();
        return "ok";
    }
}
