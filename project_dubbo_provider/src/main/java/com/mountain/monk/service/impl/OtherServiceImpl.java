package com.mountain.monk.service.impl;

import com.mountain.monk.IMemberService;
import com.mountain.monk.OtherService;
import org.apache.dubbo.config.annotation.DubboService;

@DubboService
public class OtherServiceImpl implements OtherService {

    @Override
    public void sayHello() {

    }
}

