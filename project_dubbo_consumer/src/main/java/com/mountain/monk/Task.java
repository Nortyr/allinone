package com.mountain.monk;
import java.util.Date;

import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Task implements CommandLineRunner {
//    @DubboReference(registry = {"multiple"})
    @DubboReference(timeout = 3000,retries = 0)
    private IMemberService demoService;

    @Override
    public void run(String... args) throws Exception {
        String result = demoService.getUser();
        System.out.println("Receive result ======> " + result);

        new Thread(()-> {
            while (true) {
                try {
                    Thread.sleep(1000);
                    System.out.println(new Date() + " Receive result ======> " + demoService.getUser());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    Thread.currentThread().interrupt();
                }
            }
        }).start();
    }
}
