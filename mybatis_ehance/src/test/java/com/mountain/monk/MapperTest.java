package com.mountain.monk;

import com.github.pagehelper.Page;
import com.mountain.monk.service.TestService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MapperTest {
    @Autowired
    TestService testService;

    @Test
    public void testMapper(){

        Page<com.mountain.monk.dao.Test> page=testService.selectAll();
        System.out.println(page);
    }


}