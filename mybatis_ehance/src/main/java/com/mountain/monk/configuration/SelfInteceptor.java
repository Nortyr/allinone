package com.mountain.monk.configuration;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.page.PageMethod;
import org.apache.ibatis.cache.CacheKey;
import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;

import java.lang.reflect.Method;
import java.util.Properties;

@Intercepts(
        {
                @Signature(type = Executor.class, method = "query", args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class}),
                @Signature(type = Executor.class, method = "query", args = {MappedStatement.class, Object.class, RowBounds.class, ResultHandler.class, CacheKey.class, BoundSql.class}),
        }
)
public class SelfInteceptor implements Interceptor {
    @Override
    public Object intercept(Invocation invocation) throws Throwable {
        long start=System.currentTimeMillis();
        Object[] args = invocation.getArgs();
        MappedStatement ms = (MappedStatement) args[0];
        Object parameter = args[1];
        ResultHandler resultHandler = (ResultHandler) args[3];
        Executor executor = (Executor) invocation.getTarget();
        RowBounds rowBounds = (RowBounds) args[2];

        String msId = ms.getId();
        Configuration configuration = ms.getConfiguration();
        Page page = PageHelper.getLocalPage();
        PageHelper.clearPage();

        String countMsId = msId + "_SELF";
        MappedStatement selfMs=configuration.getMappedStatement(countMsId, false);
        Object countResultList = executor.query(selfMs, parameter,rowBounds, resultHandler);
        System.out.println(countResultList);
        Class clazz= PageMethod.class;
        Method m1 = clazz.getDeclaredMethod("setLocalPage",Page.class);
        m1.setAccessible(true);
        m1.invoke(null,page);
        //注：下面的方法可以根据自己的逻辑调用多次，在分页插件中，count 和 page 各调用了一次
        Object obj=invocation.proceed();
        long end=System.currentTimeMillis();
        System.out.println("恭喜你成功浪费了5分钟。。。。"+(end-start));
        return obj;
    }

    @Override
    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    @Override
    public void setProperties(Properties properties) {

    }
}
