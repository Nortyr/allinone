package com.mountain.monk.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.mountain.monk.dao.Test;
import com.mountain.monk.dao.TestMapper;
import com.mountain.monk.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TestServiceImpl implements TestService {
    @Autowired
    private TestMapper testMapper;
    @Override
    public Page<Test> selectAll() {
        PageHelper.offsetPage(1,1);
        return (Page<Test>) testMapper.selectAll();
    }
}
