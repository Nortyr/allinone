package com.mountain.monk.service;


import com.github.pagehelper.Page;
import com.mountain.monk.dao.Test;

import java.util.List;

public interface TestService {
     Page<Test> selectAll();
}
