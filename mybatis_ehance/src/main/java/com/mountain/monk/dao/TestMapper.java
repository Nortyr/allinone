package com.mountain.monk.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface TestMapper {
    @Select("select * from test where id =1 ")
    @Results({ @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name") })
    public List<Test> selectAll();


    @Select("select * from test where id =2 ")
    @Results({ @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name") })
    public List<Test> selectAll_SELF();
}
