package com.mountain.monk.dao;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.ibatis.annotations.Result;
import org.springframework.context.annotation.Primary;

@Data
@Getter
@Setter
@NoArgsConstructor
public class Test {
    private Integer id;
    private String name;
}
