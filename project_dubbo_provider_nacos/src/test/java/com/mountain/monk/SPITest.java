package com.mountain.monk;

import com.alibaba.dubbo.common.status.StatusChecker;
import org.apache.dubbo.common.beans.ScopeBeanExtensionInjector;
import org.apache.dubbo.common.extension.ExtensionInjector;
import org.apache.dubbo.common.extension.ExtensionLoader;
import org.apache.dubbo.rpc.Protocol;
import org.apache.dubbo.rpc.model.ApplicationModel;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.env.PropertySourceLoader;
import org.springframework.boot.logging.LoggingSystemFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.support.SpringFactoriesLoader;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Set;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SPITest {


    @Test
    public void testMapper(){
        ExtensionLoader<Protocol> loader = ApplicationModel.defaultModel().getDefaultModule().getExtensionLoader(Protocol.class);
//        ExtensionInjector injector=loader.getExtension("scopeBean");
//
//        System.out.println(injector);
        Protocol injector=loader.getAdaptiveExtension();
        System.out.println(injector);
    }

    @Test
    public void springSPITest(){
        List<PropertySourceLoader> list=SpringFactoriesLoader.loadFactories(PropertySourceLoader.class, Thread.currentThread().getContextClassLoader());
        for (PropertySourceLoader propertySourceLoader :list) {
            for (String extension : propertySourceLoader.getFileExtensions()) {
                System.out.println(extension);
            }
        }
    }


}