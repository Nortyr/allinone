package com.mountain.monk.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.mountain.monk.IMemberService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Value;

/**
 * @Author wulongbo
 * @Date 2021/1/5 19:39
 * @Version 1.0
 */
@DubboService(timeout = 3000)
public class MemberServiceImpl implements IMemberService {

    @Value("${dubbo.protocol.port}")
    private String dubboPort;


    @Override
    public String getUser() {
        System.out.println("订单服务调用会员服务...dubbo服务端口号：" + dubboPort);
        return "订单服务调用会员服务...dubbo服务端口号：" + dubboPort;
    }
}
