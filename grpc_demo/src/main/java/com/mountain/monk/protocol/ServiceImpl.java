package com.mountain.monk.protocol;

import io.grpc.stub.StreamObserver;

import java.util.concurrent.atomic.AtomicInteger;

public class ServiceImpl extends GreeterGrpc.GreeterImplBase{


    @Override
    public void responseStream(Helloworld.HelloRequest request, StreamObserver<Helloworld.HelloReply> responseObserver) {
        for (int i = 1; i <= 5; i++) {
            Helloworld.HelloReply reply = Helloworld.HelloReply.newBuilder()
                    .setMessage("瞅你咋地~~" + i)
                    .build();
            System.out.println("receive from client:"+request.getMessage());
            responseObserver.onNext(reply);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        responseObserver.onCompleted();
    }

    @Override
    public StreamObserver<Helloworld.HelloRequest> requestStream(StreamObserver<Helloworld.HelloReply> responseObserver) {
        return new StreamObserver<Helloworld.HelloRequest>() {
            private StringBuilder receivedMessages = new StringBuilder();
            AtomicInteger integer=new AtomicInteger();
            @Override
            public void onNext(Helloworld.HelloRequest helloRequest) {

                receivedMessages.append(helloRequest.getMessage()).append("瞅你咋地~~"+integer.incrementAndGet());

            }

            @Override
            public void onError(Throwable throwable) {
                System.out.println("error: " + throwable.getMessage());

            }

            @Override
            public void onCompleted() {
                Helloworld.HelloReply response = Helloworld.HelloReply.newBuilder()
                        .setMessage(receivedMessages.toString())
                        .build();
                responseObserver.onNext(response);
                responseObserver.onCompleted();

            }
        };
    }



    @Override
    public StreamObserver<Helloworld.HelloRequest> doubleStream(StreamObserver<Helloworld.HelloReply> responseObserver) {
        return new StreamObserver<Helloworld.HelloRequest>() {
            AtomicInteger integer=new AtomicInteger();

            @Override
            public void onNext(Helloworld.HelloRequest helloRequest) {
                System.out.println(helloRequest.getMessage());

                Helloworld.HelloReply response = Helloworld.HelloReply.newBuilder()
                        .setMessage("瞅你咋地~~"+integer.incrementAndGet())
                        .build();
                responseObserver.onNext(response);
            }

            @Override
            public void onError(Throwable throwable) {
                System.out.println("error: " + throwable.getMessage());

            }

            @Override
            public void onCompleted() {
                System.out.println("Server completed");
                responseObserver.onCompleted();
            }
        };
    }
}
