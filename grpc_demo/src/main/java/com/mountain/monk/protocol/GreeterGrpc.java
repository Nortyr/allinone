package com.mountain.monk.protocol;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 * <pre>
 * The greeting service definition.
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.48.1)",
    comments = "Source: helloworld.proto")
@io.grpc.stub.annotations.GrpcGenerated
public final class GreeterGrpc {

  private GreeterGrpc() {}

  public static final String SERVICE_NAME = "protocol.Greeter";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.mountain.monk.protocol.Helloworld.HelloRequest,
      com.mountain.monk.protocol.Helloworld.HelloReply> getResponseStreamMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "responseStream",
      requestType = com.mountain.monk.protocol.Helloworld.HelloRequest.class,
      responseType = com.mountain.monk.protocol.Helloworld.HelloReply.class,
      methodType = io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
  public static io.grpc.MethodDescriptor<com.mountain.monk.protocol.Helloworld.HelloRequest,
      com.mountain.monk.protocol.Helloworld.HelloReply> getResponseStreamMethod() {
    io.grpc.MethodDescriptor<com.mountain.monk.protocol.Helloworld.HelloRequest, com.mountain.monk.protocol.Helloworld.HelloReply> getResponseStreamMethod;
    if ((getResponseStreamMethod = GreeterGrpc.getResponseStreamMethod) == null) {
      synchronized (GreeterGrpc.class) {
        if ((getResponseStreamMethod = GreeterGrpc.getResponseStreamMethod) == null) {
          GreeterGrpc.getResponseStreamMethod = getResponseStreamMethod =
              io.grpc.MethodDescriptor.<com.mountain.monk.protocol.Helloworld.HelloRequest, com.mountain.monk.protocol.Helloworld.HelloReply>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "responseStream"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.mountain.monk.protocol.Helloworld.HelloRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.mountain.monk.protocol.Helloworld.HelloReply.getDefaultInstance()))
              .setSchemaDescriptor(new GreeterMethodDescriptorSupplier("responseStream"))
              .build();
        }
      }
    }
    return getResponseStreamMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.mountain.monk.protocol.Helloworld.HelloRequest,
      com.mountain.monk.protocol.Helloworld.HelloReply> getRequestStreamMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "requestStream",
      requestType = com.mountain.monk.protocol.Helloworld.HelloRequest.class,
      responseType = com.mountain.monk.protocol.Helloworld.HelloReply.class,
      methodType = io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING)
  public static io.grpc.MethodDescriptor<com.mountain.monk.protocol.Helloworld.HelloRequest,
      com.mountain.monk.protocol.Helloworld.HelloReply> getRequestStreamMethod() {
    io.grpc.MethodDescriptor<com.mountain.monk.protocol.Helloworld.HelloRequest, com.mountain.monk.protocol.Helloworld.HelloReply> getRequestStreamMethod;
    if ((getRequestStreamMethod = GreeterGrpc.getRequestStreamMethod) == null) {
      synchronized (GreeterGrpc.class) {
        if ((getRequestStreamMethod = GreeterGrpc.getRequestStreamMethod) == null) {
          GreeterGrpc.getRequestStreamMethod = getRequestStreamMethod =
              io.grpc.MethodDescriptor.<com.mountain.monk.protocol.Helloworld.HelloRequest, com.mountain.monk.protocol.Helloworld.HelloReply>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.CLIENT_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "requestStream"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.mountain.monk.protocol.Helloworld.HelloRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.mountain.monk.protocol.Helloworld.HelloReply.getDefaultInstance()))
              .setSchemaDescriptor(new GreeterMethodDescriptorSupplier("requestStream"))
              .build();
        }
      }
    }
    return getRequestStreamMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.mountain.monk.protocol.Helloworld.HelloRequest,
      com.mountain.monk.protocol.Helloworld.HelloReply> getDoubleStreamMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "doubleStream",
      requestType = com.mountain.monk.protocol.Helloworld.HelloRequest.class,
      responseType = com.mountain.monk.protocol.Helloworld.HelloReply.class,
      methodType = io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING)
  public static io.grpc.MethodDescriptor<com.mountain.monk.protocol.Helloworld.HelloRequest,
      com.mountain.monk.protocol.Helloworld.HelloReply> getDoubleStreamMethod() {
    io.grpc.MethodDescriptor<com.mountain.monk.protocol.Helloworld.HelloRequest, com.mountain.monk.protocol.Helloworld.HelloReply> getDoubleStreamMethod;
    if ((getDoubleStreamMethod = GreeterGrpc.getDoubleStreamMethod) == null) {
      synchronized (GreeterGrpc.class) {
        if ((getDoubleStreamMethod = GreeterGrpc.getDoubleStreamMethod) == null) {
          GreeterGrpc.getDoubleStreamMethod = getDoubleStreamMethod =
              io.grpc.MethodDescriptor.<com.mountain.monk.protocol.Helloworld.HelloRequest, com.mountain.monk.protocol.Helloworld.HelloReply>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.BIDI_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "doubleStream"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.mountain.monk.protocol.Helloworld.HelloRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.mountain.monk.protocol.Helloworld.HelloReply.getDefaultInstance()))
              .setSchemaDescriptor(new GreeterMethodDescriptorSupplier("doubleStream"))
              .build();
        }
      }
    }
    return getDoubleStreamMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static GreeterStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<GreeterStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<GreeterStub>() {
        @java.lang.Override
        public GreeterStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new GreeterStub(channel, callOptions);
        }
      };
    return GreeterStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static GreeterBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<GreeterBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<GreeterBlockingStub>() {
        @java.lang.Override
        public GreeterBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new GreeterBlockingStub(channel, callOptions);
        }
      };
    return GreeterBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static GreeterFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<GreeterFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<GreeterFutureStub>() {
        @java.lang.Override
        public GreeterFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new GreeterFutureStub(channel, callOptions);
        }
      };
    return GreeterFutureStub.newStub(factory, channel);
  }

  /**
   * <pre>
   * The greeting service definition.
   * </pre>
   */
  public static abstract class GreeterImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     * Sends a greeting
     * </pre>
     */
    public void responseStream(com.mountain.monk.protocol.Helloworld.HelloRequest request,
        io.grpc.stub.StreamObserver<com.mountain.monk.protocol.Helloworld.HelloReply> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getResponseStreamMethod(), responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<com.mountain.monk.protocol.Helloworld.HelloRequest> requestStream(
        io.grpc.stub.StreamObserver<com.mountain.monk.protocol.Helloworld.HelloReply> responseObserver) {
      return io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall(getRequestStreamMethod(), responseObserver);
    }

    /**
     * <pre>
     * Sends another greeting
     * </pre>
     */
    public io.grpc.stub.StreamObserver<com.mountain.monk.protocol.Helloworld.HelloRequest> doubleStream(
        io.grpc.stub.StreamObserver<com.mountain.monk.protocol.Helloworld.HelloReply> responseObserver) {
      return io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall(getDoubleStreamMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getResponseStreamMethod(),
            io.grpc.stub.ServerCalls.asyncServerStreamingCall(
              new MethodHandlers<
                com.mountain.monk.protocol.Helloworld.HelloRequest,
                com.mountain.monk.protocol.Helloworld.HelloReply>(
                  this, METHODID_RESPONSE_STREAM)))
          .addMethod(
            getRequestStreamMethod(),
            io.grpc.stub.ServerCalls.asyncClientStreamingCall(
              new MethodHandlers<
                com.mountain.monk.protocol.Helloworld.HelloRequest,
                com.mountain.monk.protocol.Helloworld.HelloReply>(
                  this, METHODID_REQUEST_STREAM)))
          .addMethod(
            getDoubleStreamMethod(),
            io.grpc.stub.ServerCalls.asyncBidiStreamingCall(
              new MethodHandlers<
                com.mountain.monk.protocol.Helloworld.HelloRequest,
                com.mountain.monk.protocol.Helloworld.HelloReply>(
                  this, METHODID_DOUBLE_STREAM)))
          .build();
    }
  }

  /**
   * <pre>
   * The greeting service definition.
   * </pre>
   */
  public static final class GreeterStub extends io.grpc.stub.AbstractAsyncStub<GreeterStub> {
    private GreeterStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected GreeterStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new GreeterStub(channel, callOptions);
    }

    /**
     * <pre>
     * Sends a greeting
     * </pre>
     */
    public void responseStream(com.mountain.monk.protocol.Helloworld.HelloRequest request,
        io.grpc.stub.StreamObserver<com.mountain.monk.protocol.Helloworld.HelloReply> responseObserver) {
      io.grpc.stub.ClientCalls.asyncServerStreamingCall(
          getChannel().newCall(getResponseStreamMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public io.grpc.stub.StreamObserver<com.mountain.monk.protocol.Helloworld.HelloRequest> requestStream(
        io.grpc.stub.StreamObserver<com.mountain.monk.protocol.Helloworld.HelloReply> responseObserver) {
      return io.grpc.stub.ClientCalls.asyncClientStreamingCall(
          getChannel().newCall(getRequestStreamMethod(), getCallOptions()), responseObserver);
    }

    /**
     * <pre>
     * Sends another greeting
     * </pre>
     */
    public io.grpc.stub.StreamObserver<com.mountain.monk.protocol.Helloworld.HelloRequest> doubleStream(
        io.grpc.stub.StreamObserver<com.mountain.monk.protocol.Helloworld.HelloReply> responseObserver) {
      return io.grpc.stub.ClientCalls.asyncBidiStreamingCall(
          getChannel().newCall(getDoubleStreamMethod(), getCallOptions()), responseObserver);
    }
  }

  /**
   * <pre>
   * The greeting service definition.
   * </pre>
   */
  public static final class GreeterBlockingStub extends io.grpc.stub.AbstractBlockingStub<GreeterBlockingStub> {
    private GreeterBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected GreeterBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new GreeterBlockingStub(channel, callOptions);
    }

    /**
     * <pre>
     * Sends a greeting
     * </pre>
     */
    public java.util.Iterator<com.mountain.monk.protocol.Helloworld.HelloReply> responseStream(
        com.mountain.monk.protocol.Helloworld.HelloRequest request) {
      return io.grpc.stub.ClientCalls.blockingServerStreamingCall(
          getChannel(), getResponseStreamMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * The greeting service definition.
   * </pre>
   */
  public static final class GreeterFutureStub extends io.grpc.stub.AbstractFutureStub<GreeterFutureStub> {
    private GreeterFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected GreeterFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new GreeterFutureStub(channel, callOptions);
    }
  }

  private static final int METHODID_RESPONSE_STREAM = 0;
  private static final int METHODID_REQUEST_STREAM = 1;
  private static final int METHODID_DOUBLE_STREAM = 2;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final GreeterImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(GreeterImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_RESPONSE_STREAM:
          serviceImpl.responseStream((com.mountain.monk.protocol.Helloworld.HelloRequest) request,
              (io.grpc.stub.StreamObserver<com.mountain.monk.protocol.Helloworld.HelloReply>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_REQUEST_STREAM:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.requestStream(
              (io.grpc.stub.StreamObserver<com.mountain.monk.protocol.Helloworld.HelloReply>) responseObserver);
        case METHODID_DOUBLE_STREAM:
          return (io.grpc.stub.StreamObserver<Req>) serviceImpl.doubleStream(
              (io.grpc.stub.StreamObserver<com.mountain.monk.protocol.Helloworld.HelloReply>) responseObserver);
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class GreeterBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    GreeterBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.mountain.monk.protocol.Helloworld.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("Greeter");
    }
  }

  private static final class GreeterFileDescriptorSupplier
      extends GreeterBaseDescriptorSupplier {
    GreeterFileDescriptorSupplier() {}
  }

  private static final class GreeterMethodDescriptorSupplier
      extends GreeterBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    GreeterMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (GreeterGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new GreeterFileDescriptorSupplier())
              .addMethod(getResponseStreamMethod())
              .addMethod(getRequestStreamMethod())
              .addMethod(getDoubleStreamMethod())
              .build();
        }
      }
    }
    return result;
  }
}
