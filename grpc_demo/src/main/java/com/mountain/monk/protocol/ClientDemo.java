package com.mountain.monk.protocol;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;

public class ClientDemo {
    public static void main(String[] args) throws InterruptedException {

//        testResponseStream();
//        testRequestStream();
        testDoubleStream();
    }

    private static void testDoubleStream() throws InterruptedException {
        String host = "127.0.0.1";
        int port = 9091;
        ManagedChannel channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext().build();

//        client接收一个对象
        GreeterGrpc.GreeterStub stub = GreeterGrpc.newStub(channel);


        StreamObserver<Helloworld.HelloRequest> doubleStream = stub.doubleStream(new StreamObserver<Helloworld.HelloReply>() {

            @Override
            public void onNext(Helloworld.HelloReply helloReply) {
                System.out.println("Received: " + helloReply.getMessage());
            }

            @Override
            public void onError(Throwable throwable) {
                System.out.println("error: " + throwable);
            }

            @Override
            public void onCompleted() {
                System.out.println("Client completed");
            }
        });
        doubleStream.onNext(Helloworld.HelloRequest.newBuilder().setMessage("你瞅啥~~ 1").build());
        doubleStream.onNext(Helloworld.HelloRequest.newBuilder().setMessage("你瞅啥~~ 2").build());
        doubleStream.onNext(Helloworld.HelloRequest.newBuilder().setMessage("你瞅啥~~ 3").build());
        doubleStream.onCompleted();
        Thread.sleep(10000);
        channel.shutdown();
    }

    public static void testResponseStream() throws InterruptedException {
        String host = "127.0.0.1";
        int port = 9091;
        ManagedChannel channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext().build();

//        client接收一个对象
        GreeterGrpc.GreeterStub stub = GreeterGrpc.newStub(channel);

        stub.responseStream(Helloworld.HelloRequest.newBuilder().setMessage("你瞅啥~~").build(),
            new StreamObserver<Helloworld.HelloReply>() {
                @Override
                public void onNext(Helloworld.HelloReply helloReply) {
                    System.out.println("receive from server: " + helloReply.getMessage());
                }

                @Override
                public void onError(Throwable throwable) {
                    System.out.println("error: " + throwable);

                }

                @Override
                public void onCompleted() {
                    System.out.println("Server Streaming Completed");

                }
            });
        Thread.sleep(10000);
        channel.shutdown();
    }

    public static void testRequestStream() throws InterruptedException {
        String host = "127.0.0.1";
        int port = 9091;
        ManagedChannel channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext().build();

//        client接收一个对象
        GreeterGrpc.GreeterStub stub = GreeterGrpc.newStub(channel);


        StreamObserver<Helloworld.HelloRequest> clientObserver = stub.requestStream(new StreamObserver<Helloworld.HelloReply>() {
            @Override
            public void onNext(Helloworld.HelloReply helloReply) {
                System.out.println("Received from server : " + helloReply.getMessage());
            }
            @Override
            public void onError(Throwable throwable) {
                System.out.println("error: " + throwable);
            }
            @Override
            public void onCompleted() {
                System.out.println("Server Streaming Completed");
            }
        });
        clientObserver.onNext(Helloworld.HelloRequest.newBuilder().setMessage("你瞅啥~~1").build());
        clientObserver.onNext(Helloworld.HelloRequest.newBuilder().setMessage("你瞅啥~~2").build());
        clientObserver.onNext(Helloworld.HelloRequest.newBuilder().setMessage("你瞅啥~~3").build());
        clientObserver.onCompleted();
        Thread.sleep(10000);
        channel.shutdown();
    }


}
